/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package prjava00.montia;

		import java.net.InetAddress;
		import java.net.UnknownHostException;
/**
 *
 * @author ubu64
 */
public class Prjava00Montia {

    /**
     * @param args the command line arguments
     */
	public static void main(String[] args) {
		// TODO code application logic here
		System.out.println("Projecte de Nom Cognom");
		System.out.println("versió 0.2 del projecte prjava00-cognom");
		try {
			InetAddress addr = InetAddress.getLocalHost();
			byte[] ipAddr = addr.getAddress();
			String hostname = addr.getHostName();
			System.out.println("hostname="+hostname);
			System.out.println("Nom de l'usuari: " + System.getProperty("user.name"));
			System.out.println("Carpeta Personal: " + System.getProperty("user.home"));
			System.out.println("Sistema operatiu: " + System.getProperty("os.name"));
			System.out.println("Versió OS: " + System.getProperty("os.version"));
		}
		catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}
    
}
